const http = require('http');

const port = 4000;

const server = http.createServer((req, res) => {

	if(req.url === '/' && req.method === 'GET'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Hi, welcome back.')
	} else if(req.url === '/profile' && req.method === 'GET'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end("Let's go over your profile.")
	} else if(req.url === '/courses' && req.method === 'GET'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end("Here's our available courses.")
	} else if(req.url === '/addCourse' && req.method === 'POST'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end("This will be used for adding courses.")
	} else if(req.url === '/updateCourse' && req.method === 'PUT'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('This will be used for updating courses.')
	} else if(req.url === '/archiveCourse' && req.method === 'PATCH'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end("This will be used for archiving courses.")
	}
})

server.listen(port);
console.log(`Server is now connected at localhost:${port}.`);