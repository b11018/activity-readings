console.log(`activity-reading JP`)

// Practice For Loops

// 1.
for(let i = 0; i <= 15; i++){
	if(i % 2 === 0){
		console.log(`${i} is even`)
	} else {
		console.log(`${i} is odd`)
	}
}

// 2.
for(let i = 1; i <= 100; i++){
	if(i % 3 === 0 && i % 5 === 0){
		console.log(`FizzBuzz`)
	} else if(i % 3 === 0){
		console.log(`Fizz`)
	} else if(i % 5 === 0){
		console.log(`Buzz`)
	} else {
		console.log(i)
	}
}

// a. Create an addStudent() function
let students = ['Dean Winchester', 'Sam Winchester']

function addStudent(newStudent){
	students.push(newStudent)
}
addStudent(`Adam Millagan`)
console.log(`These are the new students: ${students}`)

// b. Create a countStudents() function
function countStudents(){
	console.log(students.length)
}
countStudents()

// c. Create a printStudents() function

function printStudents(){
	students.sort()
	students.forEach(function(student){
		console.log(student)
	})
}
printStudents()

// d. Create a findStudent() function

function findStudent(studentName){
	let filteredStudents = students.filter(function(student){
		return student.toLowerCase().includes(studentName.toLowerCase())
	})
	console.log(filteredStudents)
	if (filteredStudents.length === 1){
		console.log(`${filteredStudents} is an enrollee.`)
	} else if (filteredStudents.length > 1){
		console.log(`${filteredStudents} are enrollees.`)
	} else if (filteredStudents.length === 0){
		console.log(`${studentName} is not an enrollee.`)
	}
}
findStudent('Dean Winchester')
findStudent('winchester')
findStudent('Castiel')